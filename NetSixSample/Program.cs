using Microsoft.OpenApi.Models;
using NetSixSample;
using NetSixSample.Enum;
using Serilog;

var builder = WebApplication
    .CreateBuilder(args);

// Add services to the container.

builder.WebHost
    .ConfigureAppConfiguration((context, configBuilder) => {
        configBuilder.AddJsonFile("logsettings.json");
        LogConfig.InitLog(configBuilder.Build());
    })
    .UseSerilog();

builder.Services.AddControllers();
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new() { Title = "NetSixSample", Version = "v1" });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (builder.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "NetSixSample v1"));
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
