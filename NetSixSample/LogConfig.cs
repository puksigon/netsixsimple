﻿using Serilog;
using Microsoft.Extensions.Configuration;

namespace NetSixSample;
public static class LogConfig
{
    public static void InitLog(IConfigurationRoot config) 
    {
        try
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();

            Log.Information("Log Created.");
        }
        catch (Exception e)
        {
            Log.Debug("Log Error! : " + e.ToString());
        }
    }
}
