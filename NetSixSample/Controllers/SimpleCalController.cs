﻿
using Microsoft.AspNetCore.Mvc;

namespace NetSixSample.Controllers;

[ApiController]
[Route("[controller]")]
public class SimpleCalController : ControllerBase
{
    [HttpGet]
    public IActionResult GetCal(int first, int second) 
    {
        return Ok((first) *(second));
    }
}
