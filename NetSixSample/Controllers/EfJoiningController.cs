﻿
using Microsoft.AspNetCore.Mvc;

namespace NetSixSample.Controllers;

[ApiController]
[Route("[controller]")]
public class EfJoiningController : ControllerBase
{
    record Course(int id, string name, int categoryId);
    record CourseCategory(int id, string name);

    [HttpGet]
    public IActionResult GetJoinObj() 
    {
        var list1 = new List<Course>() {new Course(1, "Marketing Online", 2) };
        var list2 = new List<CourseCategory>() { new CourseCategory(2, "Marketing") };

        var list3 = list1.Join(list2, a => a.categoryId, b => b.id, (l1, l2) => new { l1, l2 });

        return Ok(list3);
    }
}
