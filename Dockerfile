#See https://aka.ms/containerfastmode to understand how Visual Studio uses this Dockerfile to build your images for faster debugging.

FROM mcr.microsoft.com/dotnet/aspnet:6.0 AS base
WORKDIR /app
EXPOSE 80
EXPOSE 443
ARG Configuration=Debug
ARG Semver

FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build
ARG Configuration=Debug
WORKDIR /src
COPY ["NetSixSample/NetSixSample.csproj", "NetSixSample/"]

RUN dotnet restore "NetSixSample/NetSixSample.csproj" --no-cache
COPY . .
RUN ls -la && ls -la NetSixSample
WORKDIR "/src/NetSixSample"
RUN dotnet build "NetSixSample.csproj" -c $Configuration -o /app/build

FROM build AS publish
ARG Configuration=Debug
RUN dotnet publish "NetSixSample.csproj" -c $Configuration -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "NetSixSample.dll"]